/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui;

import com.jstype.fx.EnsureIdentifier;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.Node;
import com.jstype.web.client.event.Event;
import com.jstype.web.client.event.EventHandler;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.event.dom.ContextMenuHandler;
import com.jstype.web.client.event.dom.DoubleClickHandler;
import com.jstype.web.client.event.dom.HasAllKeyHandlers;
import com.jstype.web.client.event.dom.HasAllMouseHandlers;
import com.jstype.web.client.event.dom.HasClickHandlers;
import com.jstype.web.client.event.dom.HasContextMenuHandlers;
import com.jstype.web.client.event.dom.HasDoubleClickHandlers;
import com.jstype.web.client.event.dom.KeyDownHandler;
import com.jstype.web.client.event.dom.KeyPressHandler;
import com.jstype.web.client.event.dom.KeyUpHandler;
import com.jstype.web.client.event.dom.MouseDownHandler;
import com.jstype.web.client.event.dom.MouseMoveHandler;
import com.jstype.web.client.event.dom.MouseOutHandler;
import com.jstype.web.client.event.dom.MouseOverHandler;
import com.jstype.web.client.event.dom.MouseUpHandler;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;


/**
 * Base class of all UI Controls. 
 * Every UIControl has one dom element as root node , all the elements,content are inside the root element
 * <div>
 *   <div id='txtDiv'></div>
 * </div>
 * @author Deepak Patil
 *
 */
public abstract class UIControl implements
        HasAllKeyHandlers,
        HasAllMouseHandlers,                
        HasClickHandlers,
        HasContextMenuHandlers,
        HasDoubleClickHandlers
        {

    //Do not change this name used in compiler
    //@ViewElement //NOTE:Fails in checking compiler
    @EnsureIdentifier
    private Element elm = null;
    private boolean disabled=false;

    /**
     * Default constructor for UIControl
     */
    public UIControl() {
        disabled=false;
    }
    
    /**
     * @skip;
     */
    private final void __iv(){}

    /**
     * Get the access to root element of the control
     * @return Element Native HTMLElement 
     */
    public final Element getElement() {
        return elm;
    }
    
    /**
     * Check if argument node is root node of UIControl
     * @param e DOM node instance
     * @return true if UIControl instance is associated with it
     */
    public static native boolean hasUIControl(Node e)/*-{        
        return e != null && e._wo_!=undefined;
     }-*/;
    
    /**
     * Gets the instance of UIControl associated with the DOM Node
     * @param e DOM node 
     * @return reference to the UIControl , returns <code>null</code> if no reference of UIControl
     */
    public static native UIControl getUIControl(Node e)/*-{
        return typeof e == "object" &&  e._wo_ ? e._wo_  : null;     
     }-*/;
    
    /**
     * Changes the root element of UIControl
     * @param other reference to the other root element
     */
    protected final void setElement(Element other){
        elm=other;
    }

    /**
     * Adds specified css class to the root element
     * 
     * ViewMethod Use use
     * <code>
     * <pre>
     * 	&lt;WAF:Button&gt;
     * 		&lt;addCssClassName&gt; new_css_class &lt;/addCssClassName&gt;
     * 	&lt;/WAF:Button&gt;
     * </pre> 
     * </code>
     * @param name name of the css class to add
     */
    @ViewMethod(name="addCssClassName")
    public void addCssClassName(String name) {
        elm.addCssClassName(name);
    }

    @ViewAttribute(name="cssClassName")
    public void setCssClassName(String name) {
        elm.setCssClassName(name);
    }
    
    @ViewAttribute(name="anotherCssClassName")
    public void setAnotherCssClassName(String name) {
        elm.addCssClassName(name);
    }

    public String getCssClassName() {
        return elm.getCssClassName();
    }

    public String[] getCssClassNames() {
        return elm.getCssClassNames();
    }

    public void removeCssClassName(String name) {
        elm.removeCssClassName(name);
    }

    public int getOffsetHeight() {
        return elm.getOffsetHeight();
    }

    public int getOffsetWidth() {
        return elm.getOffsetWidth();
    }

    public String getTitle() {
        return elm.getTitle();
    }

    @ViewAttribute(name="title")
    public void setTitle(String title) {
        elm.setTitle(title);
    }

    @ViewAttribute(name="height")
    public void setHeight(String height) {
        elm.getStyle().setHeight(height);
    }

   
    public void setHeight(int height) {
        elm.getStyle().setHeight(height + "px");
    }

    public String getHeight() {
        return elm.getStyle().getHeight();
    }

    @ViewAttribute(name="width")
    public void setWidth(String width) {
        elm.getStyle().setWidth(width);
    }

    public void setWidth(int width) {
        elm.getStyle().setWidth(width + "px");
    }

    public String getWidth() {
        return elm.getStyle().getWidth();
    }

    public void setSize(int height, int width) {
        elm.getStyle().setHeight(height + "px");
        elm.getStyle().setWidth(width + "px");
    }

    public void setSize(String height, String width) {
        elm.getStyle().setHeight(height);
        elm.getStyle().setWidth(width);
    }
    
    public void setDisabled(boolean value){
        disabled=value;
    }
    
    public boolean isDisabled(){
        return disabled;
    }
    
    public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
        return elm.addKeyDownHandler(handler);
    }

    public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
        return elm.addKeyUpHandler(handler);
    }

    public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
        return elm.addKeyPressHandler(handler);
    }

    public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
        return elm.addMouseDownHandler(handler);
    }

    public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
        return elm.addMouseMoveHandler(handler);
    }

    public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
        return elm.addMouseOutHandler(handler);
    }

    public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
        return elm.addMouseOverHandler(handler);
    }

    public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
        return elm.addMouseUpHandler(handler);
    }
    

    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return elm.addClickHandler(handler);
    }

    public HandlerRegistration addContextMenuHandler(ContextMenuHandler handler) {
        return elm.addContextMenuHandler(handler);
    }

    public HandlerRegistration addDoubleClickHandler(DoubleClickHandler handler) {
        return elm.addDoubleClickHandler(handler);
    }


    public void fireEvent(Event<?> event) {
        elm.fireEvent(event);
    }

    public void removeEventHandler(EventHandler handler) {
        elm.removeEventHandler(handler);
    }

    public boolean hasEventHandlersForType(String type) {
        return elm.hasEventHandlersForType(type);
    }
}
