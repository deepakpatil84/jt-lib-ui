/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;


import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;

public abstract class BasicControl extends UIControl {

    public static String DIR_LTR="ltr";
    public static String DIR_RTL="rtl";

    @ViewAttribute
    public void setName(String name) {
        getElement().setProperty("name", name);
    }
    public String getName() {
        return getElement().getPropertyString("name");
    }
    
    @ViewAttribute
    public void setId(String id){
        getElement().setId(id);
    }
    public String getId(){
        return getElement().getId();
    }
    @ViewAttribute
    public void setValue(String value) {
        getElement().setProperty("value", value);
    }
    public String getValue() {
        return getElement().getPropertyString("value");
    }
   
    public void setAccessKey(char key){
        getElement().setProperty("accesskey", key);
    }
    public char getAccessKey(){
        return (char)getElement().getPropertyInt("accesskey");
    }
    
    @ViewAttribute
    public void setDir(String dir){
        getElement().setDir(dir);
    }
    public String getDir(){
        return getElement().getDir();
    }
    public void setLeftToRight(){
        getElement().setDir(DIR_LTR);
    }
    public void setRightToLeft(){
        getElement().setDir( DIR_RTL);
    }
    
    @ViewAttribute
    public void setTabIndex(int index){
        getElement().setTabIndex(index);
    }
    public int getTabIndex(){        
        return getElement().getTabIndex();
    }
  
}
