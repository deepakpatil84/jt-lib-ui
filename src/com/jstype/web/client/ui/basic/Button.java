/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.ui.basic;

import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;

public class Button extends InputControl {
    private static String CSS_NORMAL="waf-basic-button";
    private static String CSS_DISABLED="waf-basic-button-disabled";

    public Button() {
        setText("Button");
    }

    public Button(String text) {
        setText(text);
    }

    @ViewAttribute
    @ViewMethod
    public final void setText(String text) {
        setValue(text);
    }

    public String getText() {
        return getValue();
    }

    @Override
    public void setDisabled(boolean value) {
        super.setDisabled(value);
        if (value) {
            getElement().setCssClassName(CSS_DISABLED);
        } else {
            getElement().setCssClassName(CSS_NORMAL);
        }
    }
    
    @ViewAttribute
    @ViewMethod
    public void setDisabled(String value) {
        setDisabled(Boolean.parseBoolean(value));
    }
}
