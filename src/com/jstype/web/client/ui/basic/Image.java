/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;

import com.jstype.web.client.ui.annotation.ViewAttribute;

public class Image  extends InputControl{
    @ViewAttribute
	public void setAlt(String text){
         getElement().setProperty("alt", text);
	}
	public String getAlt(){
        return getElement().getPropertyString("alt");
	}
    @ViewAttribute
    public void setSrc(String src){
        getElement().setProperty("src", src);
    }
    public String getSrc(){
        return getElement().getPropertyString("src");
    }
}
