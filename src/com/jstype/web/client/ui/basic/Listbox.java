/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;

import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.BlurHandler;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.FocusHandler;
import com.jstype.web.client.event.dom.HasBlurHandlers;
import com.jstype.web.client.event.dom.HasChangeHandlers;
import com.jstype.web.client.event.dom.HasFocusHandlers;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.Client;
import com.jstype.web.client.dom.SelectElement;
import com.jstype.web.client.dom.Style;

import java.util.ArrayList;
import java.util.Iterator;

public class Listbox extends BasicControl implements Iterable<ListboxItem>,
HasChangeHandlers, HasFocusHandlers, HasBlurHandlers{


    private static String CSS_NORMAL="waf-basic-listbox";
    private static String CSS_DISABLED="waf-basic-listbox-disabled";
    private ArrayList<ListboxItem> elements;
    private SelectElement selElem;
    private boolean disabled;
    public Listbox(){
        selElem=(SelectElement)getElement();
        elements=new ArrayList<ListboxItem>();
        disabled=false;
    }
    public Listbox(int size){
        this();
        setSize(size);
    }
    @ViewMethod
    public void addItem(String text){
        addItem(new ListboxItem(text));
    }
    @ViewMethod
    public void addItem(String text,String value){
        addItem(new ListboxItem(text, value));        
    }
    public void addItem(ListboxItem item){
        elements.add(item);
        /**
         * IE has rendering bug for <select> .it displays only first character when created dynamically
         */
        if(Client.IE){
            String ow=null;
            Style s=getElement().getStyle();
            ow=s.getWidth();
            if(ow!=null && ow.length()>0){
                s.setWidth("auto");
                getElement().appendChild(item.getElement());
                s.setWidth(ow);
            }else{
                getElement().appendChild(item.getElement());
            }
        }else{
            getElement().appendChild(item.getElement());
        }
        
        
    }
    public ListboxItem getItemAt(int index){
        if(index< 0|| index>=elements.size()){
            throw new IndexOutOfBoundsException("Index out of bounds "+index+" in Listbox::getElementAt");
        }
        return elements.get(index);
    }
    public void clear(){
        int i=0,l=elements.size();
        for(i=0;i<l;i++){
            removeItemtAt(0);
        }
    }
    public void removeItemtAt(int index){
        if(index< 0|| index>=elements.size()){
            throw new IndexOutOfBoundsException("Index out of bounds "+index+" in Listbox::getElementAt");
        }
        ListboxItem item=elements.get(index);
        getElement().removeChild(item.getElement());
        elements.remove(index);
    }
    public int getItemCount(){
        return elements.size();
    }
    @ViewAttribute
    public final void setSize(int size){
        selElem.setSize(size);
    }
    public int getSize(){
        return selElem.getSize();
    }
    @ViewAttribute
    public void setMultiple(boolean value){
        selElem.setMultiple(value);
    }
    public boolean getMultiple(){
        return getElement().getPropertyBoolean("multiple");
    }
    public int getSelectedIndex(){
        return selElem.getSelectedIndex();        
    }
    public void setSelectedValue(String value){
        for(ListboxItem item:elements){
            if(item.getValue()!=null && item.getValue().equals(value)){
                item.setSelected(true);
                break;
            }
        }
    }
    public void setSelectedIndex(int index){
        selElem.setSelectedIndex(index);
    }
    public int getSelectedCount(){
        int count=0;
        for(ListboxItem item:elements){
           if(item.isSelected())
               count++;        
        }
        return count;
    }
    public void clearSelection(){
        for(ListboxItem item:elements){
            item.setSelected(false);
        }
    }
    public ArrayList<ListboxItem> getSelectedItems(){
        ArrayList<ListboxItem> selected=new ArrayList<ListboxItem>();
        for(ListboxItem item:elements){
            if(item.isSelected()){
                selected.add(item);
            }
        }
        return selected;
    }
    public ListboxItem getSelectedItem(){
        return elements.get(selElem.getSelectedIndex());
    }
    public Iterator<ListboxItem> iterator() {
        return elements.iterator();
    }
    @ViewAttribute
    public void setDisabled(boolean value){
        selElem.setCssClassName(value ? CSS_DISABLED : CSS_NORMAL);
        selElem.setDisabled(value);
        disabled=value;        
    }
    
    @Override
    public boolean isDisabled(){
	return disabled;
    }
    public HandlerRegistration addChangeHandler(ChangeHandler handler){
	return getElement().addChangeHandler(handler);
    }
    
    public HandlerRegistration addFocusHandler(FocusHandler handler){
	return getElement().addFocusHandler(handler);
    }
    
    public HandlerRegistration addBlurHandler(BlurHandler handler){
	return getElement().addBlurHandler(handler);
    }
}
