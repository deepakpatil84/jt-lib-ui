/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.OptionElement;
public class ListboxItem extends UIControl {
    public static String CSS_NORMAL="waf-basic-listboxitem";
    public static String CSS_DISABLED="waf-basic-listboxitem-disabled";
    private OptionElement elmOpt;
    public ListboxItem(){
     
        elmOpt=(OptionElement)Window.getDocument().createElement(OptionElement.TAG);
        setElement(elmOpt);
        setText("Option");
        
    }
    public ListboxItem(String text){
        this();
        setText(text);
    }
    public ListboxItem(String text,String value){
        this();
        setText(text);
        setValue(value);        
    }
    public final void setText(String text){
        elmOpt.setText(text);
        
    }
    public String getText(){
        return elmOpt.getText();
    }
    public final void setValue(String text){
        elmOpt.setValue(text);
    }
    public String getValue(){
        return elmOpt.getValue();
    }
    public final void setSelected(boolean value){
        elmOpt.setSelected(value);
    }
    public final boolean isSelected(){
        return elmOpt.isSelected();
    }
    public final boolean getSelected(){
        return elmOpt.isSelected();
    }
}
