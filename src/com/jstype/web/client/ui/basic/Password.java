/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;

import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;

public class Password extends InputControl {
	 private static String CSS_NORMAL="waf-basic-password";
	    private static String CSS_DISABLED="waf-basic-password-disabled";
    public Password() {
        setText("");
    }

    public Password(String text) {
        setText(text);
    }

    @ViewAttribute
    public final void setText(String text) {
        setValue(text);
    }

    public String getText() {
    	String v=getValue();
        return v==null?"":v;
    }
    @ViewAttribute
    public void setMaxLength(int len){
        getElement().setProperty("maxLength",len);
    }
    public int getMaxLength(){
        return getElement().getPropertyInt("maxLength");
    }
    @ViewAttribute
    public void setReadOnly(boolean v){
        getElement().setProperty("readOnly", v);
    }
    public boolean getReadOnly(){
        return getElement().getPropertyBoolean("readOnly");
    }
    @ViewAttribute
    public void setSize(int size){
        getElement().setProperty("size", size);
    }
    public int getSize(){
        return getElement().getPropertyInt("size");
    }
    
    @Override
    public void setDisabled(boolean value){
        super.setDisabled(value);
        if(value){
            getElement().setCssClassName(CSS_DISABLED);
        }else{
            getElement().setCssClassName(CSS_NORMAL);
        }
    }
    @ViewMethod
    public void setDisabled(String value){
        setDisabled(Boolean.parseBoolean(value));
    }
}
