/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;

import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.InputElement;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.BlurHandler;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.FocusHandler;
import com.jstype.web.client.event.dom.HasBlurHandlers;
import com.jstype.web.client.event.dom.HasChangeHandlers;
import com.jstype.web.client.event.dom.HasFocusHandlers;

public final class Radio extends BasicControl implements
HasChangeHandlers, HasFocusHandlers, HasBlurHandlers{

    private InputElement elmChk;
    private Element elmLbl;
    private static String  CSS_NORMAL="waf-basic-radio";
    private static String  CSS_DISABLED="waf-basic-radio-disabled";
    

    public Radio() {
        setText("Radio");
        /*
        elmLbl.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                __clickInput(elmChk);
            }
        }); 
        */
    }

    public Radio(String text) {
        this();
        setText(text);
    }

    @ViewAttribute
    public void setGroupname(String name){
        elmChk.setName(name);
    }
    public String getGroupname(){
        return elmChk.getName();
    }

    @ViewAttribute
    public void setDisabled(boolean value){
        if(value){
            getElement().setCssClassName(CSS_DISABLED);
        }else{
            getElement().setCssClassName(CSS_NORMAL);
        }
        elmChk.setDisabled(value);
    }
    @ViewAttribute
    public void setDisabled(String value){
        setDisabled(Boolean.parseBoolean(value));
    }

    public InputElement getCheckboxElement() {
        return elmChk;
    }

    public Element getLabelElement() {
        return elmLbl;
    }

    @ViewAttribute
    public void setText(String text) {
        elmLbl.setInnerHTML(text);
    }

    @ViewMethod
    public void setText(Element element){
        elmLbl.clearChilds();
        elmLbl.appendChild(element);
    }

    public String getText() {
        return elmLbl.getInnerHTML();
    }

    @ViewAttribute
    public void setChecked(boolean v) {
        elmChk.setChecked(v);
    }

    public boolean getChecked() {
        return elmChk.isChecked();
    }
    
    public HandlerRegistration addChangeHandler(ChangeHandler handler){
	return elmChk.addChangeHandler(handler);
    }
    
    public HandlerRegistration addFocusHandler(FocusHandler handler){
	return elmChk.addFocusHandler(handler);
    }
    
    public HandlerRegistration addBlurHandler(BlurHandler handler){
	return elmChk.addBlurHandler(handler);
    }

    private native void __clickInput(InputElement e)/*-{
    if(e.disabled==false){
    e.checked=true;
    }
    }-*/;
}
