/*
 * Copyright 2011 JsType.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.list;

import com.jstype.web.client.event.logical.SelectionChangeHandler;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.TableElement;
import com.jstype.web.client.event.EventManager;
import com.jstype.web.client.event.logical.SelectionChangeEvent;

import java.util.ArrayList;

public abstract class AbstractUIList extends UIControl {
	protected ArrayList elements;
	protected static String CSS_NORMAL = "item";
	protected static String CSS_SELECTED = "item-selected";
	protected EventManager eventmanager;

	protected AbstractUIList() {
		elements = new ArrayList();
	}

	public void addElement(String elm) {
		insertElementAt(elements.size()).appendChild(
		        Window.getDocument().createTextNode(elm));
		elements.add(elm);
	}

	public void addElement(Element elm) {
		insertElementAt(elements.size()).appendChild(elm);
		elements.add(elm);
	}

	public void addElement(UIControl elm) {
		insertElementAt(elements.size()).appendChild(elm.getElement());
		elements.add(elm);
	}

	public void addElement(int index, String elm) {
		insertElementAt(index).appendChild(
		        Window.getDocument().createTextNode(elm));
		elements.add(elm);
	}

	public void addElement(int index, Element elm) {
		insertElementAt(index).appendChild(elm);
		elements.add(elm);
	}

	public void addElement(int index, UIControl elm) {
		insertElementAt(index).appendChild(elm.getElement());
		elements.add(elm);
	}

	public void addSelectionChangeHandler(SelectionChangeHandler handler) {
		if (eventmanager == null) {
			eventmanager = new EventManager();
		}
		eventmanager.addEventHandler(SelectionChangeEvent.NAME, handler);
	}

	public void clear() {
		elements.clear();
		getElement().clearChilds();
	}

	public abstract boolean isSelected(int index);

	public void select(int index) {
		checkIndex(index);
		((DivElement) getElement().getChildAt(index))
		        .setCssClassName(CSS_SELECTED);
	}

	public void deselect(int index) {
		checkIndex(index);
		((DivElement) getElement().getChildAt(index))
		        .setCssClassName(CSS_NORMAL);
	}

	protected int getItemDivIndex(DivElement div) {

		DivElement root = (DivElement) getElement();
		int size = root.getChildNodes().length;
		int index = -1;

		for (int i = 0; i < size; i++) {
			if (root.getChildAt(i) == div) {
				index = i;
				break;
			}
		}
		return index;
	}

	protected abstract DivElement insertElementAt(int index);

	public ArrayList<Object> getSelectedItems() {
		int l = elements.size();
		ArrayList<Object> al = new ArrayList<Object>();
		for (int i = 0; i < l; i++) {
			if (isSelected(i)) {
				al.add(elements.get(i));
			}
		}
		return al;
	}

	public int getSelectedItemCount() {
		int l = elements.size();
		int count = 0;
		for (int i = 0; i < l; i++) {
			if (isSelected(i)) {
				count++;
			}
		}
		return count;
	}

	public void clearSelection() {
		int l = elements.size();
		for (int i = 0; i < l; i++) {
			deselect(i);
		}
	}

	public void selectAll() {
		int l = elements.size();
		for (int i = 0; i < l; i++) {
			select(i);
		}
	}

	public int getElementCount() {
		return elements.size();
	}

	public Object removeElementAt(int index) {
		checkIndex(index);
		return elements.remove(index);
	}

	public boolean removeElement(Object obj) {
		return elements.remove(obj);
	}

	protected void checkIndex(int index) {
		if (index < 0 || index >= elements.size()) {
			throw new IndexOutOfBoundsException("Index out of bounds index:"
			        + index + " size:" + elements.size());
		}
	}

	public Object getElementAt(int index) {
		checkIndex(index);
		return elements.get(index);
	}

	public int indexOf(Object obj) {
		return elements.indexOf(obj);
	}

}
