/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


package com.jstype.web.client.ui.panels;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Style;
import com.jstype.web.client.dom.Element;

import java.util.ArrayList;
import java.util.Iterator;



public class AbsolutePanel extends SinglePanel{
    
    
    public AbsolutePanel(){        
    }
    
    public void addElement(Element elm,int x,int y){
        elements.add(elm);
        getElement().appendChild(elm);
        Style s=elm.getStyle();
        s.setPosition("absolute");
        s.setLeft(""+x+"px");
        s.setTop(""+y+"px");
    }
    public void addElement(UIControl control,int x,int y){
        elements.add(control);
        getElement().appendChild(control.getElement());
        Style s=control.getElement().getStyle();
        s.setPosition("absolute");
        s.setLeft(""+x+"px");
        s.setTop(""+y+"px");
    }
   
    public DivElement getContentDiv(){
    	return (DivElement)getElement();
    }
    
   

}
