/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.panels;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class ExpandPanel extends SinglePanel{
    
    private boolean expanded;
    @ViewElement
    private DivElement divTitle;
    @ViewElement
    private DivElement divIcon;
    @ViewElement
    private DivElement divTitleText;
    @ViewElement
    private DivElement divContent;

    private static String CSS_ICON_COLLAPSED="icon-collapsed";
    private static String CSS_ICON_EXPANDED="icon-expanded";
    
    public ExpandPanel(){
        expanded=false;
        
        divTitle.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if(expanded){
                    collapse();
                }else{
                    expand();
                }
            }
        });
    }
    public ExpandPanel(String text){
        this();
        setTitle(text);
    }
    
    public DivElement getContentDiv(){
    	return divContent;
    }
    
    @Override
    @ViewAttribute
    public final void setTitle(String text){
        divTitleText.setInnerHTML(text);
    }
    
    @Override
    public String getTitle(){
       return divTitleText.getInnerHTML();
    }

    @ViewMethod
    public void setTitle(Element element){
        divTitleText.clearChilds();
        divTitleText.appendChild(element);
    }    
    
    public void expand(){
        if(!expanded){
            divContent.getStyle().setDisplay("block");
            divIcon.setCssClassName(CSS_ICON_EXPANDED);
            expanded=true;
        }
    }
    public void collapse(){
        if(expanded){
            divContent.getStyle().setDisplay("none");
            divIcon.setCssClassName(CSS_ICON_COLLAPSED);
            expanded=false;
        }
    }
    
    public boolean isExpanded(){
        return expanded;
    }
    
    public boolean isCollapsed(){
        return !expanded;
    }   
      
}
