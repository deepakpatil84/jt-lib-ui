/*
 * Copyright 2011 JsType.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.ui.panels;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class StackPanel extends UIControl implements
	Iterable<StackPanelElement> {

    private ArrayList<StackPanelElement> elements;

    private int selectedIndex;
    public static String CSS_TITLE = "title";
    public static String CSS_ELEMENT_NORMAL = "element";
    public static String CSS_ELEMENT_SELECTED = "element-selected";
    public static String CSS_CONTENT = "content";
   

    public StackPanel() {
	selectedIndex = -1;
	elements = new ArrayList<StackPanelElement>();
    }

    @ViewMethod
    public void addElement(final StackPanelElement tab) {
	getElement().appendChild(tab.getElement());
	tab.getTitleDiv().addClickHandler(new ClickHandler() {

	    public void onClick(ClickEvent event) {
		select(tab);
	    }
	});	
	tab.getContentDiv().setCssClassName(CSS_CONTENT);
	tab.getTitleDiv().setCssClassName(CSS_TITLE);
	elements.add(tab);
	if (elements.size() == 1) {
	    select(0);
	}
    }

    public void addElement(int index, final StackPanelElement tab) {
	if (index < 0 || index >= elements.size()) {
	    throw new IndexOutOfBoundsException("Index:" + index + ",Size:"
		    + elements.size());
	}
	StackPanelElement ex = elements.get(index);	
	getElement().insertBefore(tab.getElement(), ex.getElement());
	tab.getContentDiv().setCssClassName(CSS_CONTENT);
	tab.getTitleDiv().setCssClassName(CSS_TITLE);
	elements.add(index, ex);
    }

    private void select(int index) {
	if (selectedIndex >= 0) {
	    StackPanelElement elm = elements.get(selectedIndex);
	    elm.getElement().setCssClassName(CSS_ELEMENT_NORMAL);	    
	}
	StackPanelElement elm = elements.get(index);
	elm.getElement().setCssClassName(CSS_ELEMENT_SELECTED);	
	selectedIndex = index;
    }

    private void select(StackPanelElement tab) {
	int index = getIndex(tab);
	if (index >= 0) {
	    select(index);
	}
    }

    public int getIndex(StackPanelElement tab) {
	int rvalue = -1;
	int count = 0;
	for (StackPanelElement t : elements) {
	    if (t.equals(tab)) {
		rvalue = count;
		break;
	    }
	    count += 1;
	}
	return rvalue;
    }

    public Iterator<StackPanelElement> iterator() {
	return elements.iterator();
    }
}
