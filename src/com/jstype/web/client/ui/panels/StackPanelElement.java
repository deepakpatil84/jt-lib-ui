/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


package com.jstype.web.client.ui.panels;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;

import java.util.ArrayList;
import java.util.Iterator;

public class StackPanelElement  extends SinglePanel{   

    @ViewElement
    private DivElement divTitle;
    @ViewElement
    private DivElement divContent;
    
    public StackPanelElement(){        
    }
    
    public StackPanelElement(String text){
        this();
        setTitle(text);
    }
    
    public DivElement getContentDiv(){
	return divContent;
    }
    
    public DivElement getTitleDiv(){
        return divTitle;
    }
    
    @Override
    @ViewAttribute
    public final void setTitle(String text){
        divTitle.setInnerHTML(text);
    }
    
    @ViewMethod
    public void setTitle(Element element){
        divTitle.clearChilds();
        divTitle.appendChild(element);
    }
    
    @Override
    public String getTitle(){
        return divTitle.getInnerHTML();
    }
    
}
