/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.panels;


import com.jstype.web.client.dom.Element;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Document;

import java.util.ArrayList;
import java.util.Iterator;

public class VerticalPanel extends UIControl implements Iterable<Object>{

         
    private ArrayList<Object> elements;
    
   
    public VerticalPanel(){
        elements=new ArrayList<Object>();
    }
       
   private void insertElement(int pos,Element elm){
	   DivElement div=(DivElement)Window.getDocument().createElement(DivElement.TAG);
	   div.appendChild(elm);
	   if(pos < 0  ){
		  this.getElement().appendChild(div);
		   elements.add(elm);
	   }else{		
		   this.getElement().insertChildAt(pos, div);		   
		   elements.add(pos, elm);
	   }
	   
	  
   }
   @ViewMethod
    public void addElement(Element elm){
        insertElement(-1, elm);
    }
    @ViewMethod
    public void addElement(UIControl c){        
    	insertElement(-1,c.getElement());   
    }
    public void addElement(int index,Element elm){
        if(index<0 || index>=elements.size()){
            throw new IndexOutOfBoundsException("Index out of bounds VerticalPanel::addElementAt");
        }        
        insertElement(index, elm);
    }
    public void addElement(int index,UIControl c){
    	if(index<0 || index>=elements.size()){
            throw new IndexOutOfBoundsException("Index out of bounds VerticalPanel::addElementAt");
        }
    	insertElement(index,c.getElement());
    }
    
    public int getElementCount(){
        return elements.size();
    }
    
    public int indexOf(Object o){
        return elements.indexOf(o);
    }
    public int lasIndexOf(Object o){
        return elements.lastIndexOf(o);
    }
    
    public Object getElement(int i){
        if(i>=0 && i<elements.size()){
            return elements.get(i);
        }
        
        throw new IndexOutOfBoundsException("Index out of bounds in VerticalPanel::getElementAt");
    }
    public boolean contains(Object o){
        return elements.contains(o);
    }  
    public boolean removeElement(Object o){
        int i=elements.indexOf(o);
        if(i>=0){
            elements.remove(o);
            this.getElement().removeChildAt(i);            
        }
        return i>=0;
    }
    public boolean removeElement(int index){
        if(index<0 || index>=elements.size()){
            throw new IndexOutOfBoundsException("Index out of bounds at VerticalPanel::removeElementAt");
        }
        elements.remove(index);
        return this.getElement().removeChildAt(index)!=null;
    }
    public boolean isEmpty(){
        return elements.isEmpty();
    }
    public void clear(){
    	elements.clear();
        this.getElement().clearChilds();
    }
    public Iterator<Object> iterator() {
        return elements.iterator();
    }
    
}
