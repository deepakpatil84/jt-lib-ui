package com.jstype.web.client.ui.widget;

public interface MenuContainer {
    void hideChilds();
    MenuBase.Orientation getOrientation();
    void menuShown();

}
