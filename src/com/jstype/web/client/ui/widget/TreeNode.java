/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.ui.widget;

import com.jstype.web.client.ui.ElementNotFoundException;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.event.dom.ContextMenuHandler;
import com.jstype.web.client.event.dom.DoubleClickHandler;
import com.jstype.web.client.event.dom.KeyDownHandler;
import com.jstype.web.client.event.dom.KeyPressHandler;
import com.jstype.web.client.event.dom.KeyUpHandler;
import com.jstype.web.client.event.dom.MouseDownHandler;
import com.jstype.web.client.event.dom.MouseMoveHandler;
import com.jstype.web.client.event.dom.MouseOutHandler;
import com.jstype.web.client.event.dom.MouseOverHandler;
import com.jstype.web.client.event.dom.MouseUpHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class TreeNode extends UIControl implements Iterable<TreeNode> {

    @ViewElement
    private DivElement divStateImg;
    @ViewElement
    private DivElement divNode;
    @ViewElement
    private DivElement divChilds;
    @ViewElement
    private DivElement divText;
    private ArrayList<TreeNode> childs;
    private boolean expanded;
    private boolean expandHandlerAdded;    

    public TreeNode() {
    }
    
    public TreeNode(String text) {
        setText(text);
    }    

    private void beforeAddChild() {
        //just to optimize
        if (expandHandlerAdded == false) {
            childs = new ArrayList<TreeNode>();
            divStateImg.addClickHandler(new ClickHandler() {

                public void onClick(ClickEvent event) {
                    if (expanded) {
                        collapse();
                    } else {
                        expand();
                    }
                }
            });
            expandHandlerAdded = true;
        }
    }

    public boolean hasChild() {
        if (childs == null) {
            return false;
        }
        return !childs.isEmpty();
    }

    @ViewMethod
    public void addChild(TreeNode tn) {
        if (childs == null) {
            beforeAddChild();
        }     
        childs.add(tn);
        divChilds.appendChild(tn.getElement());
        updateIcon();
    }

    public void addChild(int index, TreeNode tn) {
        if (childs == null) {
            beforeAddChild();
        }
        if (index < 0 || index > childs.size()) {
            throw new IndexOutOfBoundsException("Index out of range in TreeNode::addChildAt");
        }        
        childs.add(tn);
        divChilds.appendChild(tn.getElement());
        updateIcon();
    }

    public TreeNode getChild(int index) {
        if (index < 0 || index > childs.size()) {
            throw new IndexOutOfBoundsException("Index out of range in TreeNode::addChildAt");
        }
        return childs.get(index);
    }

    public void removeChild(TreeNode tn) {
        int index = childs.indexOf(tn);
        if (index < 0) {
            throw new ElementNotFoundException("Element not found in child list");
        }
        removeChild(index);
    }

    public void removeChild(int index) {
        if (index < 0 || index > childs.size()) {
            throw new IndexOutOfBoundsException("Index out of range in TreeNode::addChildAt");
        }
        TreeNode tn = childs.get(index);        
        divChilds.removeChild(tn.getElement());
        updateIcon();
    }

    private void updateIcon() {        
        if (hasChild()) {
            if (expanded) {
                divStateImg.setCssClassName(TreeView.CSS_NODE_STATE_EXPANDED);
            } else {
                divStateImg.setCssClassName(TreeView.CSS_NODE_STATE_COLLAPSED);
            }
        } else {
            divStateImg.setCssClassName(TreeView.CSS_NODE_STATE_LEAF);
        }
    }

    @ViewAttribute
    public void setText(String text) {
        divText.setInnerHTML(text);
    }

    @ViewMethod
    public void setText(Element element) {
        divText.clearChilds();
        divText.appendChild(element);
    }

    public String getText() {
        return divText.getInnerHTML();
    }

    public boolean isExpanded() {
        return expanded;
    }

    @ViewAttribute
    public void expand() {
        if (!expanded) {
            divStateImg.setCssClassName(TreeView.CSS_NODE_STATE_EXPANDED);         
            divChilds.getStyle().setDisplay("");
            expanded = true;
        }
    }

    @ViewAttribute
    public void collapse() {
        if (expanded) {            
            divStateImg.setCssClassName(TreeView.CSS_NODE_STATE_COLLAPSED);            
            divChilds.getStyle().setDisplay("none");
            expanded = false;
        }
    }

    public Iterator<TreeNode> iterator() {
        if (childs == null) {
            childs = new ArrayList<TreeNode>();
        }
        return childs.iterator();
    }

    @Override
    public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
        return divText.addKeyDownHandler(handler);
    }

    @Override
    public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
        return divText.addKeyUpHandler(handler);
    }

    @Override
    public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
        return divText.addKeyPressHandler(handler);
    }

    @Override
    public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
        return divText.addMouseDownHandler(handler);
    }

    @Override
    public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
        return divText.addMouseMoveHandler(handler);
    }

    @Override
    public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
        return divText.addMouseOutHandler(handler);
    }

    @Override
    public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
        return divText.addMouseOverHandler(handler);
    }

    @Override
    public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
        return divText.addMouseUpHandler(handler);
    }

    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return divText.addClickHandler(handler);
    }

    @Override
    public HandlerRegistration addContextMenuHandler(ContextMenuHandler handler) {
        return divText.addContextMenuHandler(handler);
    }

    @Override
    public HandlerRegistration addDoubleClickHandler(DoubleClickHandler handler) {
        return divText.addDoubleClickHandler(handler);
    }
}