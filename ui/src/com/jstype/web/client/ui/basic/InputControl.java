/*
 * Copyright 2011 JsType.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;

import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.BlurHandler;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.FocusHandler;
import com.jstype.web.client.event.dom.HasBlurHandlers;
import com.jstype.web.client.event.dom.HasChangeHandlers;
import com.jstype.web.client.event.dom.HasFocusHandlers;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;


public abstract class InputControl extends BasicControl implements
	HasChangeHandlers, HasFocusHandlers, HasBlurHandlers {
    @ViewAttribute
    public void setType(String type) {
	getElement().setProperty("type", type);
    }

    public String getType() {
	return getElement().getPropertyString("type");
    }

    @Override
    @ViewAttribute
    @ViewMethod
    public void setDisabled(boolean value) {
	super.setDisabled(value);
	if (value) {
	    getElement().setProperty("disabled", "true");
	} else {
	    getElement().removeAttribute("disabled");
	}
    }
    
    public HandlerRegistration addChangeHandler(ChangeHandler handler){
	return getElement().addChangeHandler(handler);
    }
    
    public HandlerRegistration addFocusHandler(FocusHandler handler){
	return getElement().addFocusHandler(handler);
    }
    
    public HandlerRegistration addBlurHandler(BlurHandler handler){
	return getElement().addBlurHandler(handler);
    }
}
