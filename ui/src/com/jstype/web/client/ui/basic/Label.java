/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


package com.jstype.web.client.ui.basic;

import com.jstype.web.client.dom.LabelElement;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.Element;
public class Label extends BasicControl{
    private static String CSS_NORMAL="waf-basic-label";
    private static String CSS_DISABLED="waf-basic-label-disabled";
    private LabelElement lblElem;
    private boolean disabled;
    public Label(){
        disabled=false;
        setText("Label");
    }

    public Label(String text){
        disabled=false;
        setText(text);
    }
    @ViewAttribute
    public final void setText(String text){
        lblElem.setInnerHTML(text);
    }
    @ViewMethod
    public void setText(Element element){
        lblElem.clearChilds();
        lblElem.appendChild(element);
    }
    public String getText(){
        return lblElem.getInnerHTML();
    }
    @ViewAttribute
    public void setDisabled(boolean value){
        if(value){
            lblElem.setCssClassName(CSS_DISABLED);
        }else{
            lblElem.setCssClassName(CSS_NORMAL);
        }
        disabled=value;
    }
    public boolean getDisabled(){
        return disabled;
    }
}
