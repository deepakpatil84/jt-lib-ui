/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.basic;

import com.jstype.web.client.dom.TextAreaElement;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.BlurHandler;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.FocusHandler;
import com.jstype.web.client.event.dom.HasBlurHandlers;
import com.jstype.web.client.event.dom.HasChangeHandlers;
import com.jstype.web.client.event.dom.HasFocusHandlers;

public class TextArea extends BasicControl implements HasChangeHandlers, HasFocusHandlers, 
HasBlurHandlers{
	private static String CSS_NORMAL="waf-basic-textarea";
	private static String CSS_DISABLED="waf-basic-textarea-disabled";
    public TextArea(){
        setText("");
    }

    public TextArea(String text){
        setText(text);
    }
    public void setText(String text){
        ((TextAreaElement)getElement()).setValue(text);
    }
    public String getText(){
        return ((TextAreaElement)getElement()).getValue();
    }
    public void setTextSize(int rows,int cols){
        setRows(rows);
        setCols(cols);
    }
    public void setRows(int rows){
        ((TextAreaElement)getElement()).setRows(rows);
    }
    public void setCols(int cols){
        ((TextAreaElement)getElement()).setCols(cols);
    }
    public  int getRows(){
        return ((TextAreaElement)getElement()).getRows();
    }
    public int getCols(){
         return ((TextAreaElement)getElement()).getRows();
    }
    public void setDisabled(boolean value){
        ((TextAreaElement)getElement()).setDisabled(value);
        if(value){
        	this.removeCssClassName(CSS_NORMAL);
        	this.addCssClassName(CSS_DISABLED);
        }else{        	
        	this.removeCssClassName(CSS_DISABLED);
        	this.addCssClassName(CSS_NORMAL);
        }
    }
    public boolean getDisabled(){
        return ((TextAreaElement)getElement()).getDisabled();
    }
    public void setReadOnly(boolean value){
        ((TextAreaElement)getElement()).setReadOnly(value);
    }
    public boolean getReadOnly(){
        return ((TextAreaElement)getElement()).getReadOnly();
    }
    public HandlerRegistration addChangeHandler(ChangeHandler handler){
	return getElement().addChangeHandler(handler);
    }
    
    public HandlerRegistration addFocusHandler(FocusHandler handler){
	return getElement().addFocusHandler(handler);
    }
    
    public HandlerRegistration addBlurHandler(BlurHandler handler){
	return getElement().addBlurHandler(handler);
    }    

}
