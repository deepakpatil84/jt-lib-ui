/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.list;

import com.jstype.web.client.event.logical.SelectionChangeEvent;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Document;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.InputElement;
import com.jstype.web.client.dom.TableCellElement;
import com.jstype.web.client.dom.TableElement;
import com.jstype.web.client.dom.TableRowElement;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;

public class CheckboxList extends AbstractUIList {
	private static String CSS_CHECKBOX = "checkbox";
	private static String CSS_CONTENT = "content";

	public boolean isSelected(int index) {
		checkIndex(index);
		return getItemCheckbox(index).isChecked();
	}

	private InputElement getItemCheckbox(int index) {
		DivElement itemDiv = (DivElement) getElement().getChildAt(index);
		return (InputElement) itemDiv.getFirstChild().getFirstChild();

	}

	public void select(int index) {
		checkIndex(index);
		getItemCheckbox(index).setChecked(true);
		super.select(index);
	}

	public void deselect(int index) {
		checkIndex(index);
		getItemCheckbox(index).setChecked(false);
		super.deselect(index);
	}

	private void onItemDivClick(DivElement div) {
		int index = getItemDivIndex(div);
		if (index >= 0) {
			if (isSelected(index)) {
				deselect(index);
			} else {
				select(index);
			}
		}
	}

	protected DivElement insertElementAt(int index) {
		Document doc = Window.getDocument();
		final DivElement itemDiv = (DivElement) doc
		        .createElement(DivElement.TAG);
		DivElement leftDiv = (DivElement) doc.createElement(DivElement.TAG);
		DivElement rightDiv = (DivElement) doc.createElement(DivElement.TAG);
		InputElement checkbox = (InputElement) doc
		        .createElement(InputElement.TAG);
		// TODO:on ie6 we can not change the type once created
		checkbox.setType(InputElement.TYPE_CHECKBOX);
		leftDiv.appendChild(checkbox);
		leftDiv.setCssClassName(CSS_CHECKBOX);
		rightDiv.setCssClassName(CSS_CONTENT);
		itemDiv.appendChild(leftDiv);
		itemDiv.appendChild(rightDiv);

		if (elements.size() == index) {
			getElement().appendChild(itemDiv);
		} else {
			DivElement existing_div = (DivElement) getElement().getChildAt(
			        index);
			getElement().insertBefore(itemDiv, existing_div);
		}

		itemDiv.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				onItemDivClick(itemDiv);
			}
		});
		itemDiv.setCssClassName(CSS_NORMAL);

		return rightDiv;
	}
}
