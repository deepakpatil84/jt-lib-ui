/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.list;

import com.jstype.web.client.event.logical.SelectionChangeEvent;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.InputElement;
import com.jstype.web.client.dom.TableCellElement;
import com.jstype.web.client.dom.TableElement;
import com.jstype.web.client.dom.TableRowElement;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;

public class List extends AbstractUIList {

	private int selectedIndex;

	public List() {
		selectedIndex = -1;
	}

	public int getSelectedIndex() {
		return selectedIndex;
	}

	@Override
	public boolean isSelected(int index) {
		checkIndex(index);
		return selectedIndex == index;
	}

	public void select(int index) {
		checkIndex(index);
		if (selectedIndex >= 0) {
			if (selectedIndex < elements.size()) {
				((DivElement) getElement().getChildAt(selectedIndex))
				        .setCssClassName(CSS_NORMAL);
			}
		}
		selectedIndex = index;
		super.select(index);
	}

	public void deselect(int index) {
		checkIndex(index);
		selectedIndex = -1;
		super.deselect(index);
	}

	private void onItemDivClick(DivElement div) {
		int index = getItemDivIndex(div);
		if (index >= 0) {
			select(index);
		}
	}

	protected DivElement insertElementAt(int index) {
		if (index < 0 && index > elements.size()) {
			throw new IndexOutOfBoundsException("Index out of range. index:"
			        + index);
		}
		final DivElement div = (DivElement) Window.getDocument().createElement(
		        DivElement.TAG);
		if (elements.size() == index ) {
			getElement().appendChild(div);
		} else {
			DivElement existing_div = (DivElement) getElement().getChildAt(
			        index);
			getElement().insertBefore(div, existing_div);
		}
		div.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				onItemDivClick(div);
			}
		});
		div.setCssClassName(CSS_NORMAL);
		return div;
	}
}
