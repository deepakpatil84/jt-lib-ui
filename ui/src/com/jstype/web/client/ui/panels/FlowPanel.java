/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.panels;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;

public class FlowPanel extends SinglePanel {

	public FlowPanel() {
	}

	public DivElement getContentDiv() {
		return (DivElement) getElement();
	}

	@ViewMethod
	public void addElement(Element elm) {
		super.addElement(elm);
		elm.getStyle().setFloat("left");
	}

	@ViewMethod
	public void addElement(UIControl control) {
		super.addElement(control);
		control.getElement().getStyle().setFloat("left");
	}

	@ViewMethod
	public void addElement(int index, Element elm) {
		super.addElement(index, elm);
		elm.getStyle().setFloat("left");
	}

	@ViewMethod
	public void addElement(int index, UIControl control) {
		super.addElement(index, control);
		control.getElement().getStyle().setFloat("left");
	}
}
