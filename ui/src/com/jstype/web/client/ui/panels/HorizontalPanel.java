/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.ui.panels;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.TableRowElement;
import com.jstype.web.client.dom.TableCellElement;
import com.jstype.web.client.dom.TableElement;

import java.util.ArrayList;
import java.util.Iterator;

public class HorizontalPanel extends UIControl  implements Iterable<Object>{
 
    private TableRowElement trInst;
    private ArrayList<Object> elements;
    
    public HorizontalPanel() {
        elements = new ArrayList<Object>();            
    }

    
    @ViewAttribute
    public void setBorderWidth(int width){
        ((TableElement)this.getElement()).setBorder(width);        
    }
    
    public int getBorderWidth(){
        return Integer.parseInt(((TableElement)this.getElement()).getBorder());
    }
    
    @ViewMethod
    public void addElement(Element elm) {
        TableCellElement cell = trInst.insertCell(elements.size());
        elements.add(elm);
        cell.appendChild(elm);   
    }

    @ViewMethod
    public void addElement(UIControl c) {
        TableCellElement cell = trInst.insertCell(elements.size());
        elements.add(c);
        cell.appendChild(c.getElement());        
    }

    public void addElement(int index, Element elm) {
        if (index < 0 || index >= elements.size()) {
            throw new IndexOutOfBoundsException("Index out of bounds VerticalPanel::addElementAt");
        }
        TableCellElement cell = trInst.insertCell(elements.size());
        elements.add(elm);
        cell.appendChild(elm);
    }
    
    public void addElement(int index, UIControl c) {
       if(index < 0 || index >= elements.size()){
            throw new IndexOutOfBoundsException("Index out of bounds VerticalPanel::addElementAt");
       }        
       TableCellElement cell = trInst.insertCell(elements.size());
       elements.add(c);
       cell.appendChild(c.getElement());
    }

    public int getElementCount() {
        return elements.size();
    }

    public int indexOf(Object o) {
        return elements.indexOf(o);
    }

    public int lasIndexOf(Object o) {
        return elements.lastIndexOf(o);
    }

    public Object getElement(int i) {
        if (i >= 0 && i < elements.size()) {
            return elements.get(i);
        }
        throw new IndexOutOfBoundsException("Index out of bounds in VerticalPanel::getElementAt");
    }

    public boolean contains(Object o) {
        return elements.contains(o);
    }
  
    public boolean removeElement(Object o) {
        int i = elements.indexOf(o);
        if (i >= 0) {
            elements.remove(o);
            trInst.deleteCell(i);
            return true;
        }
        return false;
    }

    public boolean removeElement(int index) {
        if (index < 0 || index >= elements.size()) {
            throw new IndexOutOfBoundsException("Index out of bounds at VerticalPanel::removeElementAt");
        }
        Object o = elements.get(index);
        trInst.deleteCell(index);
        elements.remove(o);
        return true;
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }

    public void clear() {
        
        int i, len = elements.size();
        for (i = len - 1; i >= 0; i--) {
            trInst.deleteCell(i);
        }
        elements.clear();
    }

    public Iterator<Object> iterator() {
        return elements.iterator();
    }
}
