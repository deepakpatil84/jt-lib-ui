/*
 * Copyright 2011 JsType.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.panels;

import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.Node;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.MouseDownEvent;
import com.jstype.web.client.event.dom.MouseDownHandler;
import com.jstype.web.client.event.dom.MouseMoveEvent;
import com.jstype.web.client.event.dom.MouseMoveHandler;
import com.jstype.web.client.event.dom.MouseUpEvent;
import com.jstype.web.client.event.dom.MouseUpHandler;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.Window;

import java.util.ArrayList;

public class HorizontalSplitPanel extends UIControl {

    @ViewElement
    private DivElement divContainer;
    @ViewElement
    private DivElement divLeft;
    @ViewElement
    private DivElement divSplitter;
    @ViewElement
    private DivElement divRight;

    private ArrayList<Object> leftelements;
    private ArrayList<Object> rightelements;
    private HandlerRegistration mousemoveHandleReg;
    private HandlerRegistration mouseupHandleReg;

    private int first_x;
    private int first_left_width;
    private boolean resizing;

    public HorizontalSplitPanel() {
	leftelements = new ArrayList<Object>();
	rightelements = new ArrayList<Object>();
	divSplitter.addMouseDownHandler(new MouseDownHandler() {
	    public void onMouseDown(MouseDownEvent event) {
		if (resizing)
		    return;
		onSplitterMouseDown(event);
		event.getNativeEvent().stopPropagation();
		event.getNativeEvent().preventDefault();
	    }
	});
    }

    public void setLeftPanelPercentageWidth(int perc) {

	if (perc > 0 && perc < 100) {
	    int lwidth, twidth;
	    twidth = this.divContainer.getClientWidth()
		    - this.divSplitter.getOffsetWidth();
	    lwidth = Math.round((twidth * perc) / 100);
	    this.divLeft.getStyle().setWidth("" + lwidth + "px");
	    this.divSplitter.getStyle().setLeft(
		    "" + divLeft.getOffsetWidth() + "px");
	    this.divRight.getStyle().setLeft(
		    ""
			    + (divLeft.getOffsetWidth() + divSplitter
				    .getOffsetWidth()) + "px");
	}
    }

    @ViewMethod
    public void addLeftElement(UIControl control) {
	divLeft.appendChild(control.getElement());
	leftelements.add(control);
    }

    @ViewMethod
    public void addLeftElement(int index, UIControl control) {
	if (index < 0 || index > leftelements.size()) {
	    throw new IndexOutOfBoundsException("Index:" + index + ",Size:"
		    + leftelements.size());
	}
	Node elm = divLeft.getChildAt(index);
	leftelements.add(index, control);
	divLeft.insertBefore(control.getElement(), elm);
    }

    public boolean removeLeftElement(UIControl control) {
	boolean rvalue = false;

	if (leftelements.contains(control)) {

	    leftelements.remove(control);
	    divLeft.removeChild(control.getElement());
	    rvalue = true;
	}
	return rvalue;
    }

    public boolean removeLeftElement(int index) {
	if (index < 0 || index > leftelements.size()) {
	    throw new IndexOutOfBoundsException("Index:" + index + ",Size:"
		    + leftelements.size());
	}
	leftelements.remove(index);
	divLeft.removeChild(divLeft.getChildAt(index));
	return true;
    }

    @ViewMethod
    public void addLeftElement(Element element) {
	divLeft.appendChild(element);
	leftelements.add(element);
    }

    @ViewMethod
    public void addLeftElement(int index, Element element) {
	if (index < 0 || index > leftelements.size()) {
	    throw new IndexOutOfBoundsException("Index:" + index + ",Size:"
		    + leftelements.size());
	}
	Node elm = divLeft.getChildAt(index);
	leftelements.add(index, element);
	divLeft.insertBefore(element, elm);
    }

    public boolean removeLeftElement(Element element) {
	boolean rvalue = false;
	if (leftelements.contains(element)) {
	    leftelements.remove(element);
	    divLeft.removeChild(element);
	    rvalue = true;
	}
	return rvalue;
    }

    // for bttom
    @ViewMethod
    public void addRightElement(UIControl control) {
	divRight.appendChild(control.getElement());
	rightelements.add(control);
    }

    @ViewMethod
    public void addRightElement(int index, UIControl control) {
	if (index < 0 || index > rightelements.size()) {
	    throw new IndexOutOfBoundsException("Index:" + index + ",Size:"
		    + leftelements.size());
	}
	Node elm = divRight.getChildAt(index);
	rightelements.add(index, control);
	divRight.insertBefore(control.getElement(), elm);
    }

    public boolean removeRightElement(UIControl control) {
	boolean rvalue = false;
	if (rightelements.contains(control)) {
	    rightelements.remove(control);
	    divRight.removeChild(control.getElement());
	    rvalue = true;
	}
	return rvalue;
    }

    public boolean removeRightElement(int index) {
	if (index < 0 || index > rightelements.size()) {
	    throw new IndexOutOfBoundsException("Index:" + index + ",Size:"
		    + rightelements.size());
	}
	leftelements.remove(index);
	divRight.removeChild(divRight.getChildAt(index));
	return true;
    }

    @ViewMethod
    public void addRightElement(Element element) {
	divRight.appendChild(element);
	rightelements.add(element);
    }

    @ViewMethod
    public void addRightElement(int index, Element element) {
	if (index < 0 || index > rightelements.size()) {
	    throw new IndexOutOfBoundsException("Index:" + index + ",Size:"
		    + rightelements.size());
	}
	Node elm = divLeft.getChildAt(index);
	leftelements.add(index, element);
	divLeft.insertBefore(element, elm);
    }

    public boolean removeRightElement(Element element) {
	boolean rvalue = false;
	if (rightelements.contains(element)) {
	    rightelements.remove(element);
	    divRight.removeChild(element);
	    rvalue = true;
	}
	return rvalue;
    }

    private void onSplitterMouseDown(MouseDownEvent e) {
	resizing = true;
	first_x = e.getScreenX();
	first_left_width = divLeft.getOffsetWidth();
	
	mousemoveHandleReg = Window.getDocument().addMouseMoveHandler(
		new MouseMoveHandler() {
		    public void onMouseMove(MouseMoveEvent event) {
			event.getNativeEvent().stopPropagation();
			event.getNativeEvent().preventDefault();
			int delta = first_left_width - (first_x - event.getScreenX());
			if (delta >= 0) {
			    int v = divLeft.getOffsetWidth() + divSplitter.getOffsetWidth()
				    + divRight.getOffsetWidth();
			    v -= divSplitter.getOffsetWidth();
			    if (delta <= v) {
				divLeft.getStyle().setWidth("" + delta + "px");
				divSplitter.getStyle().setLeft("" + delta + "px");
				divRight.getStyle().setLeft(""+(delta + divSplitter.getOffsetWidth())+ "px");
			    }
			}
		    }
		});
	
	mouseupHandleReg = Window.getDocument().addMouseUpHandler(
		new MouseUpHandler() {
		    public void onMouseUp(MouseUpEvent event) {
			mousemoveHandleReg.removeHandler();
			mouseupHandleReg.removeHandler();
			resizing = false;
		    }
		});
    }
}
