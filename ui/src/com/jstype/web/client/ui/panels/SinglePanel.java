package com.jstype.web.client.ui.panels;

import java.util.ArrayList;
import java.util.Iterator;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Node;
import com.jstype.web.client.dom.Element;

public abstract class SinglePanel extends UIControl implements Iterable<Object>{
    protected ArrayList<Object> elements;
    
    protected SinglePanel(){
	elements=new ArrayList<Object>();
    }
    public Iterator<Object> iterator() {
        return elements.iterator();
    }
    
    public abstract DivElement getContentDiv();
    
    @ViewMethod
    public void addElement(Element element){
        elements.add(element);
        getContentDiv().appendChild(element);        
    }
    
    public void addElement(int index,Element element){
        if(index<0 || index>=elements.size()){
            throw new IndexOutOfBoundsException("Index:"+index+",Size:"+elements.size());
        }
        Node ex=getContentDiv().getChildAt(index);
        elements.add(index, element);
        getContentDiv().insertBefore(element, ex);
    }
    
    @ViewMethod
    public void addElement(UIControl control){
        elements.add(control);
        getContentDiv().appendChild(control.getElement());
    }
    
    public void addElement(int index,UIControl control){
        if(index<0 || index>=elements.size()){
            throw new IndexOutOfBoundsException("Index:"+index+",Size:"+elements.size());
        }
        Node ex=getContentDiv().getChildAt(index);
        elements.add(index, control);
        getContentDiv().insertBefore(control.getElement(), ex);
    }   
    
    
    public int getElementCount(){
        return elements.size();
    }
    public Object getElementAt(int i) {
        if (i >= 0 && i < elements.size()) {
            return elements.get(i);
        }
        throw new IndexOutOfBoundsException("Index out of bounds in FlowPanel::getElementAt");
    }
    public boolean removeElement(UIControl control){
        int i = elements.indexOf(control);
        if (i >= 0) {
            elements.remove(control);
            getContentDiv().removeChild(control.getElement());
            return true;
        }
        return false;
    }
    public boolean removeElement(Element element){
        int i = elements.indexOf(element);
        if (i >= 0) {
            elements.remove(element);
            getContentDiv().removeChild(element);
            return true;
        }
        return false;
    }
    public boolean removeElement(int index){
       if (index < 0 || index >= elements.size()) {
            throw new IndexOutOfBoundsException("Index out of bounds at VerticalPanel::removeElementAt");
        }
        Object o = elements.get(index);
        getContentDiv().removeChild(getContentDiv().getChildAt(index));
        elements.remove(o);
        return true;
    }
    
    public void clear() {        
        elements.clear();
        getContentDiv().clearChilds();
    }
}
