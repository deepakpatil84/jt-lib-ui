/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.panels;

import com.jstype.web.client.dom.AnchorElement;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.Node;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.Window;

import java.util.ArrayList;
import java.util.Iterator;

public class TabPanel extends UIControl implements Iterable<TabPanelElement>{
    private ArrayList<TabPanelElement> tabs;
    @ViewElement
    private DivElement divTitle;
    @ViewElement
    private DivElement divContent;
    private int selectedIndex;
    public static String CSS_TITLE_NORMAL="title";
    public static String CSS_TITLE_SELECTED="title-selected";

  

    @ViewAttribute
    public void setCssTitleGroup(String name){
        divTitle.setCssClassName(name);
    }
    public String getCssTitleGroup(){
        return divTitle.getCssClassName();
    }

    @ViewAttribute
    public void setCssContent(String name){
        divContent.setCssClassName(name);
    }
    public String getCssContent(){
        return divContent.getCssClassName();
    }
   
    public TabPanel(){
       selectedIndex=-1;
       tabs=new ArrayList<TabPanelElement>();
    }
    @ViewMethod
    public void addElement(final TabPanelElement tab){        
        AnchorElement elm=(AnchorElement)Window.getDocument().createElement(AnchorElement.TAG);                
        elm.setCssClassName(CSS_TITLE_NORMAL);
        elm.setInnerHTML(tab.getTitle());
        divTitle.appendChild(elm);
        elm.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                select(tab);
            }
        });
        tabs.add(tab);
        if(tabs.size()==1){
            select(0);
        }
    }
    public void addElement(int index,final TabPanelElement tab){
        if(index<0 || index >= tabs.size()){
            throw new IndexOutOfBoundsException("Index:"+index+",Size:"+tabs.size());
        }        
        AnchorElement elm=(AnchorElement)Window.getDocument().createElement(AnchorElement.TAG);        
        elm.setCssClassName(CSS_TITLE_NORMAL);
        elm.setInnerHTML(tab.getTitle());
        Node ex=divTitle.getChildAt(index);
        divTitle.insertBefore(elm, ex);
        elm.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                select(tab);
            }
        });
        tabs.add(index,tab);
    }

    private void select(int index){
        if(index<0 || index >= tabs.size()){
            throw new IndexOutOfBoundsException("Index:"+index+",Size:"+tabs.size());
        }
        if(selectedIndex>=0){
            ((Element)divTitle.getChildAt(selectedIndex)).setCssClassName(CSS_TITLE_NORMAL);
        }
        divContent.clearChilds();
        TabPanelElement tab=tabs.get(index);
        divContent.appendChild(tab.getElement());
        ((Element)divTitle.getChildAt(index)).setCssClassName(CSS_TITLE_SELECTED);
        selectedIndex=index;

    }
    private void select(TabPanelElement tab){
        int index=getIndex(tab);
        if(index<0){ return; }
        select(index);        
    }
    public int getIndex(TabPanelElement tab){
        int count=0;
        for(TabPanelElement t:tabs){
            if(t.equals(tab)){
                return count;
            }
            count+=1;            
        }
        return -1;
    }
    
    public Iterator<TabPanelElement> iterator(){
        return tabs.iterator();
    }

}
