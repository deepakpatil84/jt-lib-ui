/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


package com.jstype.web.client.ui.widget;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.event.dom.MouseMoveHandler;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.SpanElement;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.event.dom.MouseMoveEvent;

public class ColorPalette extends UIControl{
   
    @ViewElement
    private SpanElement spanRemove;
    @ViewElement
    private SpanElement spanColorValue;
    @ViewElement
    private DivElement divColors;
    private ColorPicker.ColorSelectHandler selectHandler;
    public ColorPalette(){
    	
        spanRemove.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if(selectHandler!=null){                    
                    selectHandler.onColorSelect("");
                }
            }
        });
        divColors.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                onColorsClick(event);
            }
        });
        divColors.addMouseMoveHandler(new MouseMoveHandler() {
            public void onMouseMove(MouseMoveEvent event) {
                onColorsMouseMove(event);
            }
        });
    }
    public void setColorSelectHandler(ColorPicker.ColorSelectHandler handler){
        selectHandler=handler;      
    }

    private void onColorsClick(ClickEvent e){
        Element elm=e.getSourceElement();
        if(elm.getTagName().equalsIgnoreCase("td")){
            if(selectHandler!=null){                
                selectHandler.onColorSelect(spanColorValue.getInnerHTML());
            }
        }
    }
    private void onColorsMouseMove(MouseMoveEvent e){
        Element elm=e.getSourceElement();
        if(elm.getTagName().equalsIgnoreCase("td")){
            String color=elm.getStyle().getBackgroundColor();
            String hashColor=color;
            //rgb(194, 123, 160)
            if(color.startsWith("rgb(")){
                color=color.substring(4);
                String[] parts=color.split(",");
                if(parts.length==3){
                    int v1,v2,v3;
                    v1=Integer.parseInt(parts[0].trim());
                    v2=Integer.parseInt(parts[1].trim());
                    String p=parts[2].trim();
                    if(p.endsWith(")")){
                        p=p.substring(0, p.length()-1);
                    }
                    v3=Integer.parseInt(p);
                    String s1=Integer.toHexString(v1);
                    String s2=Integer.toHexString(v2);
                    String s3=Integer.toHexString(v3);
                    if(s1.length()==1){
                        s1="0"+s1;
                    }
                    if(s2.length()==1){
                        s2="0"+s2;
                    }
                    if(s3.length()==1){
                        s3="0"+s3;
                    }
                    hashColor="#"+s1+s2+s3;                    
                }
            }
            hashColor=hashColor.toUpperCase();
            spanColorValue.setInnerHTML(hashColor);
        }
    }
}
