/*
 * Copyright 2011 JsType.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.widget;

import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.Client;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.Style;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.ClickEvent;

public class ColorPicker extends UIControl {
    public static interface ColorSelectHandler {
	void onColorSelect(String value);
    }

    private static ColorPalette palette;
    private HandlerRegistration clickReg;
    private boolean paletteShown;
    private ColorSelectHandler selectHandler;
    private ColorSelectHandler myColorSelectHandler;
    private String selectedColor = "#FFFFFF";

    public ColorPicker() {
	paletteShown = false;
	getElement().addClickHandler(new ClickHandler() {
	    public void onClick(ClickEvent event) {
		if (!paletteShown) {
		    event.getNativeEvent().stopPropagation();
		    event.getNativeEvent().preventDefault();
		    showPalette(event.getClientX(), event.getClientY());
		}
	    }
	});
	Style style = getElement().getStyle();
	style.setBackgroundColor(selectedColor);
	if (Client.IE && !Client.IE_8_OR_ABOVE) {
	    style.setPaddingTop("0px");
	}
    }

    @ViewMethod
    public void setSelectedColor(String color) {
	selectedColor = color;
	getElement().getStyle().setBackgroundColor(selectedColor);
    }

    public String getSelectedColor() {
	return selectedColor;
    }

    public void setColorSelectHandler(ColorSelectHandler handler) {
	selectHandler = handler;
    }

    private void showPalette(int x, int y) {
	if (palette == null) {
	    palette = new ColorPalette();
	    Style s = palette.getElement().getStyle();
	    s.setPosition("absolute");
	    s.setZIndex(10000);
	    paletteShown = false;
	    s.setMarginTop("" + 0 + "px");
	    Window.getDocument().getBodyElement()
		    .appendChild(palette.getElement());
	}
	if (!paletteShown) {
	    Style s = palette.getElement().getStyle();
	    s.clearDisplay();
	    s.setLeft("" + x + "px");
	    s.setTop("" + y + "px");
	    paletteShown = true;
	    clickReg = Window.getDocument().addClickHandler(new ClickHandler() {
		public void onClick(ClickEvent event) {
		    hidePalette();
		}
	    });
	    if (myColorSelectHandler == null) {
		myColorSelectHandler = new ColorSelectHandler() {
		    public void onColorSelect(String value) {
			setSelectedColor(value);
			if (selectHandler != null) {
			    selectHandler.onColorSelect(value);
			}
		    }
		};
	    }
	    palette.setColorSelectHandler(myColorSelectHandler);
	}
    }

    public void hidePalette() {
	if (paletteShown) {
	    clickReg.removeHandler();
	    clickReg = null;
	    palette.getElement().getStyle().setDisplay("none");
	    paletteShown = false;
	}
    }

}
