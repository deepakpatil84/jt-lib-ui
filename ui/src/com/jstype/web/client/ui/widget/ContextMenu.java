/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.widget;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.OListElement;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * @author Deepak Patil
 * 
 */
public class ContextMenu extends MenuBase {
	public ContextMenu() {
		setOrientation(Orientation.VERTICAL);
	}

	/* Hide current menu and all submenus.
	 * Gets called when , click event occurs outside menu 
	 * @see com.openwaf.web.client.ui.widget.MenuBase#hideChilds()
	 */
	protected void onOutsideEvent() {
		super.onOutsideEvent();
		hide();
	}

	/**
	 * Append root element to document.body , sets left and top of the element where menu will be shown
	 * 
	 * @param left
	 *            left of the root element
	 * @param top
	 *            top of the root element
	 */
	public void show(int left, int top) {
		Window.getDocument().getBodyElement().appendChild(this.getElement());
		getElement().getStyle().setLeft("" + left + "px");
		getElement().getStyle().setTop("" + top + "px");
		menuShown();
	}

	/**
	 * Hide the menu
	 */
	public void hide() {
		if (getElement().getParentNode() != null) {
			getElement().getParentNode().removeChild(getElement());
		}
	}
}
