/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */


package com.jstype.web.client.ui.widget;

import com.jstype.core.Timer;
import com.jstype.web.client.event.dom.KeyDownHandler;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.widget.DatePicker.DateSelectHandler;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.InputElement;
import com.jstype.web.client.event.KeyCodes;
import com.jstype.web.client.event.dom.BlurEvent;
import com.jstype.web.client.event.dom.BlurHandler;
import com.jstype.web.client.event.dom.ChangeEvent;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.event.dom.FocusEvent;
import com.jstype.web.client.event.dom.FocusHandler;
import com.jstype.web.client.event.dom.KeyDownEvent;

import java.util.Date;

public class DateBox extends UIControl{
	
	private static String CSS_NORMAL="waf-widget-datebox";
	private static String CSS_DISABLED="waf-widget-datebox-disabled";
    private Date currentdate;
    private static DatePicker datepicker;
    @ViewElement
    private InputElement  inpText;
    private Timer blurTimer;
    private boolean pickerClicked;
    private boolean dateSelected;
    public DateBox(){
        pickerClicked=false;
        dateSelected=false;
        currentdate=new Date();
        inpText.addFocusHandler(new FocusHandler() {
            public void onFocus(FocusEvent event) {
                onTextFocus();
            }
        });
        inpText.addBlurHandler(new BlurHandler() {
            public void onBlur(BlurEvent event) {
                onTextBlur();
            }
        });
        inpText.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                onTextChange();
            }
        });
        inpText.addKeyDownHandler(new KeyDownHandler() {
            public void onKeyDown(KeyDownEvent event) {
                int code=event.getNativeEvent().getKeyCode();
                if(code==KeyCodes.KEY_ESCAPE || code==KeyCodes.KEY_ENTER){
                    hideDatePciker();
                }                
            }
        });
        setDate(currentdate);
        blurTimer=new Timer() {
            @Override
            public void run() {
                if(pickerClicked==false){
                    hideDatePciker();
                    dateSelected=false;
                }else{                    
                    inpText.focus();
                }
                pickerClicked=false;
            }
        };
    }
    
    public DateBox(Date d){
        this();
        setDate(d);
    }
    private void onTextBlur(){
        if(datepicker!=null){
            blurTimer.schedule(200);
        }
    }
    private void onTextFocus(){
        if(datepicker==null){
            datepicker=new DatePicker(currentdate);
            datepicker.getElement().getStyle().setPosition("absolute");
            getElement().appendChild(datepicker.getElement());
            datepicker.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    if(dateSelected==false){
                        pickerClicked=true;
                    }
                    dateSelected=false;
                }
            });
            datepicker.setDateSelectHandler(new DatePicker.DateSelectHandler() {
                public void onDateSelect(Date d) {
                    setDate(d);
                    dateSelected=true;
                    hideDatePciker();
                }
            });
        }
        String disp=datepicker.getElement().getStyle().getDisplay();
        if(disp.length()>0){
            datepicker.setSelectedDate(inpText.getValue());
            datepicker.getElement().getStyle().setDisplay("");
        }
       
    }
    private void hideDatePciker(){
        if(datepicker!=null){           
            datepicker.getElement().getStyle().setDisplay("none");
        }
    }
    private void onTextChange(){
        try{
            Date d=new Date(inpText.getValue());
            setDate(d);
        }catch(IllegalArgumentException ex){
            setDate(currentdate);
        }catch(Exception ex){
            setDate(currentdate);
        }
    }
    public void setDate(Date date){
        if(date!=null){
            String s=DatePicker.DateString.SMALL_MONTHS[date.getMonth()];
            s+=" "+date.getDate()+",";
            s+=""+(1900+date.getYear());
            inpText.setValue(s);
            currentdate=date;
        }
    }
    public Date getDate(){
        return currentdate;
    }
    
    @Override
    public void setDisabled(boolean value){
    	super.setDisabled(value);
    	this.inpText.setDisabled(true);
    	if(value){
    		this.removeCssClassName(CSS_NORMAL);
    		this.addCssClassName(CSS_DISABLED);    		
    	}else{
    		this.removeCssClassName(CSS_DISABLED);
    		this.addCssClassName(CSS_NORMAL);    		
    	}
    }
}
