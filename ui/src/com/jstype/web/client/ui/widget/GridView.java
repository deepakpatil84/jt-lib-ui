/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.widget;

import com.jstype.web.client.dom.TableRowElement;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.dom.TableCellElement;
import com.jstype.web.client.dom.TableElement;

public class GridView extends UIControl{

    //private static final String CSS_DATA_ROW="waf-widget-grid-data-row";
    //private static final String CSS_DATA_CELL="waf-widget-grid-data-cell";
    //private static final String CSS_HEADER_CELL="waf-widget-grid-header-cell";

    @ViewElement
    private TableElement tblData;
    @ViewElement
    private TableElement tblHeader;
   
    private TableRowElement trHeader;
    @ViewElement
    private DivElement divData;
    @ViewElement
    private DivElement divHeader;

    
    public GridView(){
        trHeader=tblHeader.insertRow(0);        
    }
    public int getColumnCount(){
        return trHeader.getCells().length;
    }
    public int getDataRowCount(){
        return tblData.getRows().length;
    }
    public void setColumnCount(int count){
        int curColCount=getColumnCount();
        if(curColCount==count){
            return;
        }
        if(curColCount<count){
            while(curColCount<count){
                TableCellElement tc=trHeader.insertCell(curColCount++);
                //tc.setCssClassName(CSS_HEADER_CELL);
            }
        }else{
            while(curColCount>count){
                trHeader.deleteCell(--curColCount);
            }
        }
        float w=100/curColCount;
        for(int i=0;i<curColCount;i++){
            trHeader.getCells()[i].getStyle().setWidth(""+w+"%");
        }
    }
    private void checkColumnIndex(int index){
        if(index<0 || index>=getColumnCount()){
            throw new IndexOutOfBoundsException("Index out of bounds Index:"+index+" Available:0 to "+getColumnCount());
        }
    }
    public void setColumnWidth(int index,String width){
        checkColumnIndex(index);
        trHeader.getCells()[index].getStyle().setWidth(width);
        if(getRowCount()>0){
            tblData.getRows()[0].getCells()[index].getStyle().setWidth(width);
        }
    }
    public void setHeader(int index,String text){
        checkColumnIndex(index);
        trHeader.getCells()[index].setInnerHTML(text);
    }
    public void setHeader(int index,Element elm){
        checkColumnIndex(index);
        TableCellElement tc=trHeader.getCells()[index];
        tc.clearChilds();
        tc.appendChild(elm);
    }
    public void setHeader(int index,UIControl control){
        setHeader(index, control.getElement());
    }

    public TableCellElement getHeaderCellAt(int index){
        checkColumnIndex(index);
        return trHeader.getCells()[index];
    }
    public String getHeaderString(int index){
        checkColumnIndex(index);
        return trHeader.getCells()[index].getInnerHTML();
    }
    public Element getHeaderElement(int index){
        checkColumnIndex(index);
        return (Element)trHeader.getCells()[index].getFirstChild();
    }
    public void clearData(){
        int count=getDataRowCount();
        while(count>0){
            tblData.deleteRow(0);
            count--;
        }
    }    
    public int getRowCount(){
        return tblData.getRows().length;
    }
    public void appendRow(){
       int rowCount=getRowCount();       
       TableRowElement tr=tblData.insertRow(rowCount);
       //tr.setCssClassName(CSS_DATA_ROW);
       int colCount=getColumnCount();
       for(int i=0;i<colCount;i++){
           TableCellElement tc=tr.insertCell(i);
           //tc.setCssClassName(CSS_DATA_CELL);
       }
       if(rowCount==0){
            int curColCount=getColumnCount();            
            for(int i=0;i<curColCount;i++){
                tr.getCells()[i].getStyle().setWidth(trHeader.getCells()[i].getStyle().getWidth());
            }
        }
    }
    public void insertRowAt(int index){
       checkRowIndex(index);      
       TableRowElement tr=tblData.insertRow(index);
       //tr.setCssClassName(CSS_DATA_ROW);
       int colCount=getColumnCount();
       for(int i=0;i<colCount;i++){
           TableCellElement tc=tr.insertCell(i);
           //tc.setCssClassName(CSS_DATA_CELL);
       }
       if(index==0){
            int curColCount=getColumnCount();            
            for(int i=0;i<curColCount;i++){
                tr.getCells()[i].getStyle().setWidth(trHeader.getCells()[i].getStyle().getWidth());
            }
        }
    }
    private void checkRowIndex(int index){
        if(index<0 || index>getRowCount()){
            throw new IndexOutOfBoundsException("Index out of bounds Index:"+index+" Available:0 to "+getRowCount());
        }
    }
     public void addCellData(int row,int col,String data){
        checkColumnIndex(col);
        checkRowIndex(row);
        TableCellElement cell=tblData.getRows()[row].getCells()[col];
        cell.appendChild(Window.getDocument().createTextNode(data));
    }
    public void addCellData(int row,int col,Element elm){
        checkColumnIndex(col);
        checkRowIndex(row);
        TableCellElement tc=tblData.getRows()[row].getCells()[col];        
        tc.appendChild(elm);
    }
    public void addCellData(int row,int col,UIControl control){
        addCellData(row, col,control.getElement());
    }
    public void setCellData(int row,int col,String data){
        checkColumnIndex(col);
        checkRowIndex(row);
        tblData.getRows()[row].getCells()[col].setInnerHTML(data);
    }
    public void setCellData(int row,int col,Element elm){
        checkColumnIndex(col);
        checkRowIndex(row);
        TableCellElement tc=tblData.getRows()[row].getCells()[col];
        tc.clearChilds();
        tc.appendChild(elm);
    }
    public void setCellData(int row,int col,UIControl control){
        setCellData(row, col,control.getElement());
    }
    public TableCellElement getCellAt(int row,int col){
        checkColumnIndex(col);
        checkRowIndex(row);
        return tblData.getRows()[row].getCells()[col];
    }
    public String getCellString(int row,int col){
        checkColumnIndex(col);
        checkRowIndex(row);
        return tblData.getRows()[row].getCells()[col].getInnerHTML();        
    }
    public Element getCellElement(int row,int col){
        checkColumnIndex(col);
        checkRowIndex(row);
        return (Element)tblData.getRows()[row].getCells()[col].getFirstChild();
    }
    public DivElement getHeaderDiv(){
        return divHeader;
    }
    public DivElement getDataDiv(){
        return divData;
    }
    public void setHeaderHeight(int height){
        tblHeader.getStyle().setHeight(""+height+"px");
    }
    @ViewAttribute
    public void setHeaderDivCss(String cssText){
        divHeader.getStyle().addCssText(cssText);
    }
    @ViewAttribute
    public void setDataDivCss(String cssText){
        divData.getStyle().addCssText(cssText);
    }
    public void setDataHeight(int height){
        divData.getStyle().setHeight(""+height+"px");
    }
    @ViewAttribute
    public void setDataHeight(String height){
        divData.getStyle().setHeight(height);
    }
   
}

