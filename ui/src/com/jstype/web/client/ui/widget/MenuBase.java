/*
 * Copyright 2011 JsType.com
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.widget;

import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.Window;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.OListElement;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.ClickEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public abstract class MenuBase extends UIControl implements Iterable<MenuItem>,
	MenuContainer {

    public static String CSS_MENU_ITEM = "menu-item;";
    public static String CSS_MENU_ITEM_HOVER = "menu-item-selected;";

    private DivElement divChilds;
    private Orientation orientation;
    
    private ArrayList<MenuItem> childs;
    
    private boolean globalClickHandlerAdded;
    private ClickHandler globalClickHandler;
    HandlerRegistration globalClickReg;

    public MenuBase() {
	divChilds = (DivElement) getElement();
	globalClickHandlerAdded = false;
	childs = new ArrayList<MenuItem>();	
	orientation = Orientation.HORIZONTAL;
	globalClickHandler = new ClickHandler() {
	    public void onClick(ClickEvent event) {
		onOutsideEvent();
	    }
	};
    }

    public enum Orientation {
	HORIZONTAL, VERTICAL
    }

    @ViewAttribute
    public void setOrientation(String value) {
	if (value.equals("vertical")) {
	    setOrientation(Orientation.VERTICAL);
	} else {
	    setOrientation(Orientation.HORIZONTAL);
	}
    }

    public void setOrientation(Orientation value) {
	orientation = value;
    }

    public Orientation getOrientation() {
	return orientation;
    }

    public boolean isHorizontal() {
	return orientation == Orientation.HORIZONTAL;
    }

    public boolean isVertical() {
	return orientation == Orientation.VERTICAL;
    }

    public void menuShown() {
	if (!globalClickHandlerAdded) {
	    globalClickReg = Window.getDocument().addClickHandler(globalClickHandler);
	    globalClickHandlerAdded = true;
	}
    }
    protected void onOutsideEvent(){
	hideChilds();
	globalClickReg.removeHandler();
	globalClickHandlerAdded = false;
    }
    public void hideChilds() {
	if (childs != null) {
	    for (MenuItem item : childs) {
		item.hideChilds();
		item.hide();
	    }
	}
    }

    @ViewMethod
    public void addChild(MenuItem item) {
	childs.add(item);
	item.setParent(this);
	if (isHorizontal()) {
	    item.getElement().getStyle().setFloat("left");
	} else {
	    item.getElement().getStyle().setFloat("");
	}
	divChilds.appendChild(item.getElement());
    }

    public void addChild(int index, MenuItem item) {
	if (index < 0 || index > childs.size()) {
	    throw new IndexOutOfBoundsException(
		    "Index out of range in MenuBar::addMenuItemAt");
	}
	MenuItem exitem = childs.get(index);
	childs.add(item);
	if (isHorizontal()) {
	    item.getElement().getStyle().setFloat("left");
	} else {
	    item.getElement().getStyle().setFloat("");
	}
	item.setParent(this);
	divChilds.insertBefore(item.getElement(), exitem.getElement());
    }

    public boolean removeChild(MenuItem item) {
	if (!childs.contains(item)) {
	    return false;
	}
	childs.remove(item);
	item.setParent(null);
	return divChilds.removeChild(item.getElement()) != null;
    }

    public boolean removeChild(int index) {
	if (index < 0 || index > childs.size()) {
	    throw new IndexOutOfBoundsException(
		    "Index out of range in MenuBar::removeMenuItemAt");
	}
	return removeChild(childs.get(index));
    }

    public Iterator<MenuItem> iterator() {
	return childs.iterator();
    }

}
