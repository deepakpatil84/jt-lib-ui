/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.widget;

import com.jstype.web.client.Window;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.ui.widget.MenuBase.Orientation;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.event.dom.ContextMenuHandler;
import com.jstype.web.client.event.dom.DoubleClickHandler;
import com.jstype.web.client.event.dom.KeyDownHandler;
import com.jstype.web.client.event.dom.KeyPressHandler;
import com.jstype.web.client.event.dom.KeyUpHandler;
import com.jstype.web.client.event.dom.MouseDownHandler;
import com.jstype.web.client.event.dom.MouseMoveHandler;
import com.jstype.web.client.event.dom.MouseOutHandler;
import com.jstype.web.client.event.dom.MouseOverEvent;
import com.jstype.web.client.event.dom.MouseOverHandler;
import com.jstype.web.client.event.dom.MouseUpHandler;

import java.util.ArrayList;
import java.util.Iterator;

public class MenuItem extends UIControl implements Iterable<MenuItem>,
        MenuContainer {

	// private boolean root;
	// private int index;
	// private MenuBase menubar;
	private ArrayList<MenuItem> childs;
	private MenuContainer parent;
	// @ViewElement
	private DivElement divChilds;
	@ViewElement
	private DivElement divMenu;
	// @ViewElement
	private DivElement divImgSubMenu;
	@ViewElement
	private DivElement divText;

	public static String CCS_MENU_ITEM_NORMAL = "menu-item";
	public static String CCS_MENU_ITEM_SELECTED = "menu-item-selected";

	// public static String CCS_MENU_BAR_ITEM_NORMAL="waf-widget-menu-item";
	// public static String CCS_MENU_BAR_ITEM_SELECTED="waf-widget-menu-item-selected";

	public MenuItem() {
		// root = false;
		childs = null;// initiated this only when needed
		parent = null;
		// menubar = null;

	}

	public MenuItem(String text) {
		this();
		setText(text);
	}

	@ViewAttribute
	public final void setText(String text) {
		divText.setInnerHTML(text);
	}

	@ViewMethod
	public final void setText(Element element) {
		divText.clearChilds();
		divText.appendChild(element);
	}

	public String getText() {
		return divText.getInnerHTML();
	}

	/*
	 * Never call this
	 * @Skip
	 */
	/*
	public void setRoot(boolean value, MenuBase menubar) {
	    root = value;
	    if (value) {
	        index = 0;
	        this.menubar = menubar;
	        updateIndex();
	    }       
	}

	private boolean isRoot() {
	    return root;
	}

	private void setMenuBar(MenuBar mb) {
	    menubar = mb;
	}

	public MenuBase getMenuBar() {
	    return menubar;
	}

	public int getIndex() {
	    return index;
	}

	private void setIndex(int index) {
	    this.index = index;
	}

	private void updateIndex() {
	if (childs != null) {
	    if (!childs.isEmpty()) {
		
		if ((menubar != null && menubar.isVertical()) || root == false) {
		    divImgSubMenu.getStyle().setDisplay("");
		} else {
		    divImgSubMenu.getStyle().setDisplay("none");
		}
		
		divMenu.setCssClassName(CCS_MENU_ITEM_NORMAL);
		
		for (MenuItem item : childs) {
		    item.index = index + 1;
		    item.menubar = menubar;
		    item.updateIndex();
		}
	    } else {
		divImgSubMenu.getStyle().setDisplay("none");
	    }
	}
	}
	*/
	public void menuShown() {
		parent.menuShown();
	}

	public void hideChilds() {
		if (childs != null) {
			for (MenuItem item : childs) {
				item.hideChilds();
				item.hide();
			}
		}
	}

	public Orientation getOrientation() {
		return Orientation.VERTICAL;
	}

	protected void setParent(MenuContainer parent) {
		this.parent = parent;
	}

	protected MenuContainer getParent() {
		return parent;
	}

	@ViewMethod
	public void addChild(MenuItem item) {
		if (childs == null) {
			beforeChildAdd();
		}
		childs.add(item);
		item.setParent(this);

		divChilds.appendChild(item.getElement());
	}

	public void addChild(int index, MenuItem item) {
		if (childs == null) {
			beforeChildAdd();
		}
		if (index < 0 || index > childs.size()) {
			throw new IndexOutOfBoundsException(
			        "Index out of range in Menuitem::addChildAt");
		}
		MenuItem exitem = childs.get(index);
		childs.add(index, item);
		item.setParent(this);

		divChilds.insertBefore(item.getElement(), exitem.getElement());
	}

	private void beforeChildAdd() {
		if (childs == null) {
			/*
			<div id="divImgSubMenu" class="waf-widget-menu-submenu-image" style="position:absolute;display: none;right: 2px;" />
			<div id="divText" class="waf-widget-menu-text" style="white-space:nowrap !important;cursor:pointer;display:inline;padding-right:20px;">Menu</div>
			<div id="divChilds" class="waf-widget-menu-block" style="display:none;"/>
			*/
			childs = new ArrayList<MenuItem>();
			divMenu.addMouseOverHandler(new MouseOverHandler() {
				public void onMouseOver(MouseOverEvent event) {
					show();
				}
			});
			DivElement div = (DivElement) Window.getDocument().createElement(
			        DivElement.TAG);
			// <div id="divImgSubMenu" class="waf-widget-menu-submenu-image"
			// style="position:absolute;display: none;right: 2px;" />
			divImgSubMenu = div;
			div.setCssClassName("menu-submenu-image");
			divMenu.insertChildAt(0, div);

			div = (DivElement) Window.getDocument().createElement(
			        DivElement.TAG);
			// <div id="divChilds" class="waf-widget-menu-block" style="display:none;"/>
			divChilds = div;
			div.setCssClassName("menu-block");
			divMenu.appendChild(div);

			divChilds.getStyle().setDisplay("none");

		}

	}

	public int getChildCount() {
		/*
		if (childs == null) {
		    return 0;
		}
		return childs.size();
		*/
		return childs == null ? 0 : childs.size();
	}

	public boolean removeChild(MenuItem item) {
		if (childs == null) {
			throw new IndexOutOfBoundsException(
			        "Index out of range in Menuitem::removeChild");
		}
		if (!childs.contains(item)) {
			return false;
		}
		// item.setMenuBar(null);
		divChilds.removeChild(item.getElement());
		return childs.remove(item);
	}

	public boolean removeChild(int index) {
		if (childs == null) {
			throw new IndexOutOfBoundsException(
			        "Index out of range in Menuitem::removeChildAt");
		}
		if (index < 0 || index > childs.size()) {
			throw new IndexOutOfBoundsException(
			        "Index out of range in Menuitem::removeChildAt");
		}
		MenuItem item = childs.get(index);
		divChilds.removeChild(item.getElement());
		// item.setMenuBar(null);
		return childs.remove(index) != null;
	}

	public void hide() {
		// if(menubar.isHorizontal() && root==true){
		// divMenu.setCssClassName(CCS_MENU_BAR_ITEM_NORMAL);
		// }else{
		divMenu.setCssClassName(CCS_MENU_ITEM_NORMAL);
		// }
		if(divChilds!=null){
			divChilds.getStyle().setDisplay("none");
		}
	}

	public void show() {
		/*
		    if (isRoot()) {
		        if (menubar.isVertical()) {
		            divChilds.getStyle().setMarginLeft("100%");
		            divChilds.getStyle().setTop("0px");                
		        }
		    } else {
		        divChilds.getStyle().setMarginLeft("100%");
		        divChilds.getStyle().setTop("0px");
		    }
		    menubar.addMenuToHide(this);
		    */
		// if(menubar.isHorizontal() && root==true){
		// divMenu.setCssClassName(CCS_MENU_BAR_ITEM_SELECTED);
		// }else{
		parent.hideChilds();
		if (parent.getOrientation() == Orientation.VERTICAL) {
			divChilds.getStyle().setMarginLeft("100%");
			divChilds.getStyle().setTop("0px");
		}
		divMenu.setCssClassName(CCS_MENU_ITEM_SELECTED);
		// }
		if (childs != null && childs.isEmpty() == false) {
			divChilds.getStyle().setDisplay("");
		}
		parent.menuShown();
	}

	public Iterator<MenuItem> iterator() {
		if (childs == null) {
			beforeChildAdd();
		}
		return childs.iterator();
	}

	@Override
	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return divText.addKeyDownHandler(handler);
	}

	@Override
	public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
		return divText.addKeyUpHandler(handler);
	}

	@Override
	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return divText.addKeyPressHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
		return divText.addMouseDownHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
		return divText.addMouseMoveHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
		return divText.addMouseOutHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
		return divText.addMouseOverHandler(handler);
	}

	@Override
	public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
		return divText.addMouseUpHandler(handler);
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return divText.addClickHandler(handler);
	}

	@Override
	public HandlerRegistration addContextMenuHandler(ContextMenuHandler handler) {
		return divText.addContextMenuHandler(handler);
	}

	@Override
	public HandlerRegistration addDoubleClickHandler(DoubleClickHandler handler) {
		return divText.addDoubleClickHandler(handler);
	}
}
