/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.widget;

import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.SpanElement;

public class ProgressBar extends UIControl {
    @ViewElement
    private DivElement divContainer;
    @ViewElement
    private DivElement divValue;
    @ViewElement
    private SpanElement spanText;
    private double currentvalue;
    private double minvalue;
    private double maxvalue;
    public ProgressBar(){
        this(0,0,100);
    }
    public ProgressBar(double  currentvalue,double minvalue,double maxvalue){
    	this.minvalue=minvalue;
        this.maxvalue=maxvalue;
        this.setCurrentValue(currentvalue);
    }
    public void setCurrentValue(double value){
        if(value<minvalue || value>maxvalue){
            return;
            //throw new Exception("Out of range value Value:"+value+" Min:" +minvalue+" Max:"+maxvalue);
        }
        currentvalue=value;
        divValue.getStyle().setWidth(""+(currentvalue/maxvalue)*100+"%");
    }
    public double getCurrentValue(){
        return currentvalue;
    }
    public double getMinValue(){
        return minvalue;
    }
    public double getMaxValue(){
        return maxvalue;
    }
    public void setText(String text){
        spanText.setInnerHTML(text);
    }
    public String getText(){
        return spanText.getInnerHTML();
    }   
}
