/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.ui.widget;

import com.jstype.web.client.dom.DivElement;
import com.jstype.web.client.dom.Element;
import com.jstype.web.client.event.dom.ChangeEvent;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewElement;
import com.jstype.web.client.ui.basic.Listbox;
import com.jstype.web.client.Window;


public class RichTextEditor extends UIControl {

    @ViewElement
    private DivElement divJustCenter;
    @ViewElement
    private DivElement divStrike;
    @ViewElement
    private ColorPicker cpTextColor;
    @ViewElement
    private Listbox lbFontSize;
    @ViewElement
    private DivElement divBold;
    @ViewElement
    private DivElement divRemoveFormat;
    @ViewElement
    private ColorPicker cpBackColor;
    @ViewElement
    private DivElement divJustRight;
    @ViewElement
    private DivElement divUL;
    @ViewElement
    private DivElement divJustFull;
    @ViewElement
    private DivElement divHR;
    @ViewElement
    private DivElement divRemoveLink;
    @ViewElement
    private DivElement divIndent;
    @ViewElement
    private DivElement divUnderline;
    @ViewElement
    private DivElement divImage;
    @ViewElement
    private DivElement divJustLeft;
    @ViewElement
    private DivElement divItalic;
    @ViewElement
    private RichText rtIns;
    @ViewElement
    private DivElement divOutdent;
    @ViewElement
    private DivElement divSup;
    @ViewElement
    private Listbox lbFontName;
    @ViewElement
    private DivElement divOL;
    @ViewElement
    private DivElement divSub;
    @ViewElement
    private DivElement divInsertLink;

    public RichTextEditor() {
        lbFontName.addItem("Font", "");
        lbFontName.addItem("Normal", "inherit");
        lbFontName.addItem("Times New Roman", "Times New Roman");
        lbFontName.addItem("Arial", "Arial");
        lbFontName.addItem("Courier New", "Courier New");
        lbFontName.addItem("Georgia", "Georgia");
        lbFontName.addItem("Trebuchet", "Trebuchet");
        lbFontName.addItem("Verdana", "Verdana");
        lbFontName.addItem("Tahoma", "Tahoma");
        lbFontName.addItem("Halvetica", "Halvetica");
        lbFontName.addItem("Sans Serif", "sans-serif");

        lbFontSize.addItem("","");
        lbFontSize.addItem("XX-Small","1");
        lbFontSize.addItem("X-Small","2");
        lbFontSize.addItem("Small","3");
        lbFontSize.addItem("Medium","4");
        lbFontSize.addItem("Large","5");
        lbFontSize.addItem("X-Large","6");
        lbFontSize.addItem("XX-Large","7");
        divBold.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.toggleBold();
            }
        });
        divItalic.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.toggleItalic();
            }
        });
        divUnderline.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.toggleUnderline();
            }
        });
        divSub.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.toggleSubscript();
            }
        });
        divSup.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.toggleSubscript();
            }
        });
        divStrike.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.toggleStrikethrough();
            }
        });
        divOL.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.insertOrderedList();
            }
        });
        divUL.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.insertUnorderedList();
            }
        });
        divJustLeft.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.justify("left");
            }
        });
        divJustCenter.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.justify("center");
            }
        });
        divJustRight.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.justify("right");
            }
        });
        divJustFull.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.justify("full");
            }
        });
        divIndent.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.indent();
            }
        });
        divOutdent.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.outdent();
            }
        });
        divHR.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.insertHorizontalRule();
            }
        });
        divImage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String r=Window.prompt("Enter Absolue Path of Image", "http://");
                if(r==null) return;
                if(!r.startsWith("http://")){
                    r="http://"+r;
                }
                rtIns.insertImage(r);
            }
        });
        divInsertLink.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                String r=Window.prompt("Enter Absolue Path of Link", "http://");
                if(r==null) return;
                if(!r.startsWith("http://")){
                    r="http://"+r;
                }
                rtIns.createLink(r);
            }
        });
        divRemoveLink.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                rtIns.removeLink();
            }
        });
        divRemoveFormat.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
               rtIns.removeFormat();
            }
        });
        cpTextColor.setColorSelectHandler(new ColorPicker.ColorSelectHandler() {
            public void onColorSelect(String value) {
                 rtIns.setForeColor(value);
            }
        });
        cpBackColor.setColorSelectHandler(new ColorPicker.ColorSelectHandler() {
            public void onColorSelect(String value) {
                rtIns.setBackColor(value);
            }
        });
      
        lbFontName.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                 rtIns.setFontName(lbFontName.getSelectedItem().getValue());
            }
        });
        lbFontSize.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                rtIns.setFontSize(lbFontSize.getSelectedItem().getValue());
            }
        });
    }
    public void setHTML(Element element){
        rtIns.setHTML(element);
    }
    
    public void setHTML(String text){
        rtIns.setHTML(text);
    }
}
