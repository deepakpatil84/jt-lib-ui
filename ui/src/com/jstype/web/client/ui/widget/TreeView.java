/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.ui.widget;

import com.jstype.web.client.ui.ElementNotFoundException;
import com.jstype.web.client.ui.UIControl;
import com.jstype.web.client.ui.annotation.ViewAttribute;
import com.jstype.web.client.ui.annotation.ViewMethod;
import com.jstype.web.client.dom.DivElement;

import java.util.ArrayList;
import java.util.Iterator;

public class TreeView extends UIControl implements Iterable<TreeNode> {

    public static String CSS_TREE = "waf-widget-treeview";
    public static String CSS_CHILD_BLOCK = "block";
    public static String CSS_NODE = "treenode";
    public static String CSS_NODE_STATE_LEAF = "leaf";
    public static String CSS_NODE_STATE_COLLAPSED = "collapsed";
    public static String CSS_NODE_STATE_EXPANDED = "expanded";
    public static String CSS_TEXT = "text";
           
    private ArrayList<TreeNode> childs;
    private DivElement divChilds;

    public TreeView() {
        childs = new ArrayList<TreeNode>();
    }

    @ViewMethod
    public void addChild(TreeNode node) {
        childs.add(node);
        divChilds.appendChild(node.getElement());                       
    }

    public void addChild(int index, TreeNode node) {
        if (index < 0 || index > childs.size()) {
            throw new IndexOutOfBoundsException("Index out of range in TreeNode::addChildAt");
        }
        TreeNode ex = childs.get(index);
        childs.add(index, node);
        divChilds.insertBefore(node.getElement(), ex.getElement());                
    }

    public TreeNode getChild(int index) {
        if (index < 0 || index > childs.size()) {
            throw new IndexOutOfBoundsException("Index out of range in TreeNode::addChildAt");
        }
        return childs.get(index);
    }

    public void removeChild(TreeNode tn) {
        int index = childs.indexOf(tn);
        if (index < 0) {
            throw new ElementNotFoundException("Element not found in child list");
        }
        removeChild(index);
    }

    public void removeChild(int index) {
        if (index < 0 || index > childs.size()) {
            throw new IndexOutOfBoundsException("Index out of range in TreeNode::addChildAt");
        }
        TreeNode tn = childs.get(index);
        divChilds.removeChild(tn.getElement());        
    }

    public Iterator<TreeNode> iterator() {
        return childs.iterator();
    }
}
